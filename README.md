# koha-display-records: Wordpress plugin

## Introduction

This is a Wordpress plugin that allows the site to fetch bibliographic records from Koha using a public report. Your Wordpress site administrator will use a `[koha-display]` shortcode with various parameters to insert Koha displays anywhere in the site.

At this stage, the plugin fetches the title, biblionumber, and cover image.

It displays them in a grid across the width of the Wordpress page.

Each record will be a clickable link back to the Koha public catalogue (OPAC).

## Catalogue your records

This plugin assumes the cover image is supplied by an external link and stored in the 856$u of the record.

## Writing the report

The report must only return a column of biblionumbers.

### Example report to fetch a list

`SELECT virtualshelfcontents.biblionumber FROM virtualshelfcontents WHERE virtualshelfcontents.shelfnumber = <Report ID>`

## Install the plugin

Download the .zip file from the [Releases page](https://gitlab.com/aleishaamohia/wordpress-koha-display-records/-/releases) and upload it to your Wordpress site.

## Inserting the shortcode into Wordpress

The `[koha-display]` shortcode takes two parameters. An error will be displayed if either of those two parameters are missing or if the API request returns an error.

Example of valid shortcode:

```
[koha-display opac_base_url="https://demo.mykoha.co.nz" report_id="24"]
```

### Parameters

* **opac_base_url**: This is the base URL of your public Koha catalogue site, or OPAC. It will match the format of the OPACBaseURL Koha system preference. It must start with HTTP or HTTPS, and must NOT have a trailing slash.
* **report_id**: This is the unique ID number assigned the SQL report in Koha that returns a list of biblionumbers.

## Authors and acknowledgment

This plugin was written by Aleisha Amohia *<aleisha@catalyst.net.nz>* with sponsorship from Māoriland Film Festival

## Future work

More smartly fetch the cover image from Koha, and work for a multitude of sources.
